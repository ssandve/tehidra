---
title: "Hindra results integrated with TE-CREs"
author: "Simen"
date: "Våren 2022"
output:
  html_document:
    df_print: paged
    code_folding: hide
editor_options:
  chunk_output_type: console
---
  
<!-- output: -->
<!--   html_document: -->
<!--     toc: true -->
<!--     theme: yeti -->
<!--     code_folding: hide -->
<!--     number_sections: false -->
<!-- editor_options:  -->
<!--   chunk_output_type: console -->
  
<style>
.info { background-color:aliceblue; }
.task { background-color:lightpink; }
blockquote { font-size: 20px; }
/* https://rpubs.com/stevepowell99/floating-css */
#TOC {
  position: fixed;
  left: 0;
  top: 0;
  width: 250px;
  height: 100%;
  overflow: auto;
  background-color: #dddddd;
}
body {
  max-width: 100%;
  margin: auto;
  margin-left: 260px;
  line-height: 20px;
}
.show {
  display: block !important;
}
</style>
  
<br> 
  
### Background
  
We utilized results from a massive parallel reporter assay (MPRA), based on random DNA-sequences derived from open chromatin in liver, to interrogate the role of different TEs in regulating transcription.


```{r, echo=F, eval=T, warning=F, message=F}


library(tidyverse); library(DESeq2)

# Arrange TE-CRE file based on TOBIAS results and write a bed file
#tecres <- read_csv("input_data/TOBIAS_TE.tsv", col_names = T)
#tecres <- read_csv("input_data/TOBIAS_TEv5.1.csv", col_names = T)
tecres <- read_csv("input_data/boundmots_5.1.csv", col_names = T)
tecres$chrom <- sub('^0+', '', sub('ssa', '', tecres$chrom))
write_tsv(select(tecres, -4), file = "results_files/tecre.bed", col_names = F)


# Arrange hidra results file and write a bed file
resToDf <- function(res){
  as.data.frame(res) %>% 
    rownames_to_column(var="region") %>% as_tibble() %>% 
    separate(region, into=c("chr","start","end")) %>% 
    mutate(start=as.integer(start), end=as.integer(end))
}


hidra <- readRDS("input_data/DESeq_res_lengthBin.RDS")
hidra <- hidra %>% lapply(resToDf) %>% bind_rows(.id="bin")
write_tsv(select(hidra, 2, 3, 4, 6, 10), file = "results_files/active_elements.bed", col_names = F)

# run bedtools to intersect TE-CREs (TFBS motifs overlaping a TE) and hidra fragments. Overlap must be 100 %

system('/opt/homebrew/bin/bedtools intersect -wa -wb -f 1 -a /Users/simensandve/Dropbox/Work/Projects/Rewired/hidra/tehidra/results_files/tecre.bed -b /Users/simensandve/Dropbox/Work/Projects/Rewired/hidra/tehidra/results_files/active_elements.bed > /Users/simensandve/Dropbox/Work/Projects/Rewired/hidra/tehidra/results_files/tecre_intersect_hidra.bed')


# read in results from bedtools intersect
overlap = read_tsv('results_files/tecre_intersect_hidra.bed', col_names = F)
overlap$hidraID <- paste(overlap$X8, overlap$X9, overlap$X10, sep = '_')

# Collapse overlap table on hidraID's: 

TECRE.original <- overlap %>% 
                  group_by(hidraID) %>% 
                  summarise(log2FoldChange = unique(X11),
                            padj = unique(X12),
                            TFBS=n(), 
                            bound_TFBS_liver = sum(X5), 
                            TFBS_name = paste(X4, collapse = ','),
                            TE = paste(unique(X7),collapse = ',' )) 
                                                          

rem <- length(grep(',', TECRE.original$TE)) # removing hidra's overlapping >1 TE: 14,279 hidra frags

# remove hidra that overlap TWO TEs
TECRE <- TECRE.original[grep(',', TECRE.original$TE, invert = T), ]

write_tsv(TECRE, file = "results_files/HidraFrags_overlapping_TECREs.tsv")



```


<br> 

### Results
#### TE-TFBS overlapping with MPRA fragments
Out of `r nrow(tecres)` putative transcription factor binding sites (TFBS) residing within a TE, `r nrow(overlap)` (`r round(nrow(overlap)/nrow(hidra), 2)` %) were found to overlap a MPRA fragment (a total of `r nrow(hidra)` MPRA fragments). Highly similar TFBS (referred to as TE-TFBS) often overlap eachother on the same strand and they are also shorter (mean size of `r round(mean(tecres$end-tecres$start), 2)` bp) than the DNA-fragments in the MPRA experiment (mean size of `r round(mean(hidra$end-hidra$start), 2)` bp). Hence, each MPRA fragment contained on average `r round(mean(TECRE.original$TFBS), 2)` TE-TFBS.

<br> 

#### TEs overlapping active MRPA fragments
To explore how TE's contribute to active cis-regulatory elements (CREs), we first identified those MPRA fragments with significant impact on transcription (p-adjusted < 0.05 & abs(log2 fold change) > 0). We found that `r sum(TECRE$padj<0.05 & abs(TECRE$log2FoldChange)>0, na.rm=T)` (`r round(sum(TECRE$padj<0.05 & abs(TECRE$log2FoldChange)>0, na.rm=T)/nrow(TECRE), 3)*100`%) of the MRPA fragments impacted transcription, with `r sum(TECRE$padj<0.05 & TECRE$log2FoldChange>0, na.rm=T)` fragments inducing and `r sum(TECRE$padj<0.05 & TECRE$log2FoldChange<0, na.rm=T)` fragments repressing transcription.

<br>

<div class="task">

**DISCUSSION POINT:**  
-  should we merge overlapping MPRA fragments (from same ATAC-peak) into a single observation? 

</div>

<br>

#### Enrichment of TE-superfamilies and TE-families in active MPRA-fragments
To assess if specific TEs have contributed disproportionately to the TE-CRE landscape in Atlantic salmon we classified TE-derived MPRA fragments at the TE-superfamily- and -family level and performed fisher-exact test contrasting active (inducing or repressing transcription) vs not-active MPRA fragments. 

```{r, echo=F, eval=T}

# Two functions to perform exact test and parse results into tables

list_to_table = function(fisher.list=up_motifs[1]){
  pval = sapply(fisher.list, function(i) i$fisher[[1]])
  padj <- p.adjust(pval, method = 'fdr')
  odds.ratio = as.numeric(sapply(fisher.list, function(i) i$fisher[3]))
  conf.interval = sapply(fisher.list, function(i) paste(round(as.vector(i$fisher[2])[[1]], 3), collapse='_'))
  prop.sig = sapply(fisher.list, function(i) i[[2]])
  prop.notsig = sapply(fisher.list, function(i) i[[3]])
  counts.sig = sapply(fisher.list, function(i) i[[4]])
  counts.notsig = sapply(fisher.list, function(i) i[[5]])
  tibble(ID = names(fisher.list), pval, padj, odds.ratio, 
        conf.interval, prop.sig, prop.notsig, counts.sig,counts.notsig)
  
}


fisher_TEtest = function(test.hidra.table, notsig.hidra.table, simulate=F) {
  
  reslist <- lapply(names(notsig.hidra.table), function(i) {
    is.match = match(i, names(test.hidra.table))
    if(is.na(is.match)) {
      prop.notsig <- notsig.hidra.table[i]/sum(notsig.hidra.table)
      counts.notsig <- notsig.hidra.table[i]
      listres <- list(NA, NA, prop.notsig, NA, counts.notsig)
      names(listres) <- c("fisher", 'prop.sig', 'prop.notsig', 'counts.sig', 'counts.notsig')
      return(listres)
    }
    
    if(!is.na(is.match)) {
      fisher <- fisher.test(cbind(c(test.hidra.table[is.match], all= sum(test.hidra.table)),
            c(notsig.hidra.table[i], all=sum(notsig.hidra.table))), simulate.p.value = simulate)
      
      prop.sig <- test.hidra.table[is.match]/sum(test.hidra.table)
      prop.notsig <- notsig.hidra.table[i]/sum(notsig.hidra.table)
      counts.sig <- test.hidra.table[is.match]
      counts.notsig <- notsig.hidra.table[i]
      
      listres <- list(fisher, prop.sig, prop.notsig, counts.sig, counts.notsig)
      names(listres) <- c("fisher", 'prop.sig', 'prop.notsig', 'counts.sig', 'counts.notsig')
      return(listres)
    }
  }
  )
  
  names(reslist) <- names(notsig.hidra.table)
  reslist
  
}

```

<br>

##### Superfamily-level enrichment results

  
```{r, echo=F, eval=T}
# # add collumn with superfamily
TECRE$superfamily <- sub('_.*', '', TECRE$TE)

# # is the superfamily distribution similar to TOBIAS distribution for UP and DOWN?
# #does TE impact direction of transcription impact
# 
TE_distribution_UP <- TECRE %>% filter(padj<0.05 & log2FoldChange>0 ) %>% pull(superfamily) %>% table()
TE_distribution_DOWN <- TECRE %>% filter(padj<0.05 & log2FoldChange<0) %>% pull(superfamily) %>% table()
TE_distribution_notsig <- TECRE %>% filter(padj>0.05) %>% pull(superfamily) %>% table()
superfamilies_in_sighidra <- names(TE_distribution_notsig)[names(TE_distribution_notsig) %in% unique(c(names(TE_distribution_UP), names(TE_distribution_DOWN)))]
TE_distribution_notsig <- TE_distribution_notsig[superfamilies_in_sighidra] # removing superfamilies not in hidra

# perform fisher tests on superfamilies
UP_fisher <- fisher_TEtest(test.hidra.table = TE_distribution_UP, notsig.hidra.table = TE_distribution_notsig)
UP_fisher.table <- list_to_table(fisher.list = UP_fisher)
#UP_fisher.table %>% filter(padj<0.1)

DOWN_fisher <- fisher_TEtest(test.hidra.table = TE_distribution_DOWN, notsig.hidra.table = TE_distribution_notsig)
DOWN_fisher.table <- list_to_table(fisher.list = DOWN_fisher)
#DOWN_fisher.table %>% filter(padj<0.1)

barplot.table = right_join(DOWN_fisher.table, UP_fisher.table, by='ID') %>% 
  select(ID, propDOWN=prop.sig.x, prop.NOSIG=prop.notsig.x, propUP=prop.sig.y, padjDOWN=padj.x, padjUP=padj.y)
  
barplot.table$propDOWN[is.na(barplot.table$propDOWN)] <- 0
barplot.table$propUP[is.na(barplot.table$propUP)] <- 0  


barplot.table.ready <- barplot.table %>% select(-c(1,5:6)) %>% t()
colnames(barplot.table.ready) <- barplot.table$ID


```

The active MPRA fragments (transcriptionally inducing and repressing) overlapped a total of `r TECRE %>% filter(padj<0.05) %>% pull(superfamily) %>% unique() %>% length()` unique TE superfamilies (out of `r length(unique(TECRE$superfamily))` families in total). To test for enrichment of specific TE superfamilies in inducing or repressing MPRA fragment we performed a fisher test for each superfamilyfamily (Figure 1).

After correcting for multiple testing (fdr adjusted pvalue<0.1) `r sum(c(barplot.table$padjDOWN, barplot.table$padjUP)<0.1, na.rm=T)` superfamilies had significant `fisher.test` p-value. The unknown LTR superfamily elements [RSX](http://botserv2.uzh.ch/kelldata/trep-db/TREP_more_SF.php?code_key=RLX) was enriched for inductive fragments, while [DMM](http://botserv2.uzh.ch/kelldata/trep-db/TREP_more_SF.php?code_key=DMM) and DTS superfamilies were enriched for repressive fragments. The fourth RIN superfamily  had significantly less inductive fragments compared to random expectations.

```{r, echo=F, fig.height=5, fig.width=9, fig.cap='Figure 1: TE-superfamily distribution in active and non-active MPRA fragments. Significant fisher tests (padj<0.1) indicated with a star.'}

## plot fisher test hidra regulation 
par(mar=c(8,4,4,4))
bp <- barplot(barplot.table.ready, beside = T, col=c('skyblue', 'lightgrey' ,'firebrick'), 
        las=3, ylab='Proportion')
legend("topright",bty='n', legend=c("Induction", "Non-active", "Repression"), fill=c('firebrick','lightgrey' ,'skyblue'))
mtext(side=1, line = 5, text = "TE families significant fisher-test"  )

if(length(which(barplot.table$padjUP<0.1))>0){
  text(x = bp[3,which(barplot.table$padjUP<0.1)], 
       y = barplot.table$propUP[which(barplot.table$padjUP<0.1)]+0.02,
      labels='*', col="firebrick", cex = 1.2)
}

text(x = bp[1,which(barplot.table$padjDOWN<0.1)], 
     y = barplot.table$propDOWN[which(barplot.table$padjDOWN<0.1)]+0.02,
    labels='*', col="blue", cex = 1.2)
```

```{r, echo=F}

pdf(file = "figures/TEsuperfam_fisher.pdf",
width = 10, height=5, paper = "a4")
par(mar=c(8,4,4,4))
bp <- barplot(barplot.table.ready, beside = T, col=c('skyblue', 'lightgrey' ,'firebrick'), 
        las=3, ylab='Proportion')
legend("topright",bty='n', legend=c("Induction", "Non-active", "Repression"), fill=c('firebrick','lightgrey' ,'skyblue'))
mtext(side=1, line = 5, text = "TE families significant fisher-test"  )

if(length(which(barplot.table$padjUP<0.1))>0){
  text(x = bp[3,which(barplot.table$padjUP<0.1)], 
       y = barplot.table$propUP[which(barplot.table$padjUP<0.1)]+0.02,
      labels='*', col="firebrick", cex = 1.2)
}

text(x = bp[1,which(barplot.table$padjDOWN<0.1)], 
     y = barplot.table$propDOWN[which(barplot.table$padjDOWN<0.1)]+0.02,
    labels='*', col="blue", cex = 1.2)

dev.off()

```

<br> 

##### Family-level enrichment results
The active MPRA fragments (transcriptionally inducing and repressing) overlapped a total of `r TECRE %>% filter(padj<0.05) %>% pull(TE) %>% unique() %>% length()` unique TE families (out of `r length(unique(TECRE$TE))` families in total). To test for enrichment of specific TE families in inducing or repressing MPRA fragment we performed a fisher test for each family.

```{r, echo=F}
# preparing for testing families 
# fisher test table for families
TEfamily_distribution_UP <- TECRE %>% filter(padj<0.05 & log2FoldChange>0 ) %>% pull(TE) %>% table()
TEfamily_distribution_DOWN <- TECRE %>% filter(padj<0.05 & log2FoldChange<0 ) %>% pull(TE) %>% table()
TEfamily_distribution_notsig <- TECRE %>% filter(padj>0.05) %>% pull(TE) %>% table()
families_in_sighidra <- names(TEfamily_distribution_notsig)[names(TEfamily_distribution_notsig) %in% unique(c(names(TEfamily_distribution_UP), names(TEfamily_distribution_DOWN)))]
TEfamily_distribution_notsig <- TEfamily_distribution_notsig[families_in_sighidra]


# fisher tests TE family level 
UPfamily_fisher <- fisher_TEtest(TEfamily_distribution_UP, TEfamily_distribution_notsig)
UPfamily_table <- list_to_table(UPfamily_fisher)

DOWNfamily_fisher <- fisher_TEtest(TEfamily_distribution_DOWN, TEfamily_distribution_notsig)
DOWNfamily_table <- list_to_table(DOWNfamily_fisher)

# fix plotting table

family.barplot.table = right_join(DOWNfamily_table, UPfamily_table, by='ID') %>% 
  select(ID, propDOWN=prop.sig.x, prop.NOSIG=prop.notsig.x, propUP=prop.sig.y, padjDOWN=padj.x, padjUP=padj.y)

family.barplot.table <- family.barplot.table %>% filter(padjDOWN<0.1 | padjUP <0.1)

family.barplot.table$propDOWN[is.na(family.barplot.table$propDOWN)] <- 0
family.barplot.table$propUP[is.na(family.barplot.table$propUP)] <- 0  


family.barplot.table.ready <- family.barplot.table %>% select(-c(1,5:6)) %>% t()
colnames(family.barplot.table.ready) <- family.barplot.table$ID

TEfam <- sapply(strsplit(TECRE$TE, '_'), '[[', 2)
TEsupfam <- sapply(strsplit(TECRE$TE, '_'), '[[', 1)
TEsupfam_colnames <- TEsupfam[match(colnames(family.barplot.table.ready), TEfam)]

# 
# colnames(family.barplot.table.ready) <- paste0(TEsupfam_colnames, "_", colnames(family.barplot.table.ready))

family.barplot.table.ready <- family.barplot.table.ready[,c(order(TEsupfam_colnames))]

```



After correcting for multiple testing (fdr adjusted pvalue<0.1) `r ncol(family.barplot.table.ready)` families were enriched in  active MPRA fragments, with `r sum(DOWNfamily_table$padj<0.1, na.rm=T)` and `r sum(UPfamily_table$padj<0.1, na.rm=T)` family enriched for repressive and inducing MPRA fragments, respectively. 



```{r, echo=F, fig.height=5, fig.width=9, fig.cap="Figure 2: TE-families significantly enriched (padj<0.1) in active MPRA fragments"}

par(mar=c(10,4,4,4))
bp <- barplot(family.barplot.table.ready, beside = T, col=c('skyblue','lightgrey' ,'firebrick'), 
              las=3, ylab='Proportion', cex.names=0.9)

legend("topright",bty='n', legend=c("Induction", "Non-active", "Repression"), fill=c('firebrick','lightgrey' ,'skyblue'))

mtext(side=1, line = 8, text = "TE families significant fisher-test"  )

```

```{r, echo=F}
pdf(file = "figures/TEfam_fisher.pdf",
width = 10, height=7, paper = "a4")
par(mar=c(10,4,4,4))
bp <- barplot(family.barplot.table.ready, beside = T, col=c('skyblue','lightgrey' ,'firebrick'), 
              las=3, ylab='Proportion', cex.names=0.9)

legend("topright",bty='n', legend=c("Induction", "Non-active", "Repression"), fill=c('firebrick','lightgrey' ,'skyblue'))
mtext(side=1, line = 8, text = "TE families significant fisher-test"  )
dev.off()

```


<br>

#### Enrichment of TFBS in active MPRA fragments
To test for enrichment of particular TFBS's in active MPRA fragments we applied fisher tests (again...) for each individual TFBS. 

```{r, echo=F, eval=T, message=F}

## motif analyses #

motifs_hindra_sigUP <- overlap %>% filter(X12<0.05 & X11>0) %>% pull(X4) %>% table()
motifs_hindra_sigDOWN <- overlap %>% filter(X12<0.05 & X11<0) %>% pull(X4) %>% table()
motifs_hindra_notsig <- overlap %>% filter(X12>0.05) %>% pull(X4) %>% table()

up_motifs <- fisher_TEtest(motifs_hindra_sigUP, motifs_hindra_notsig, simulate = T) 
UPmotifs.table <- list_to_table(up_motifs)
#UPmotifs.table %>% filter(odds.ratio < 0.5 | odds.ratio >2, padj<0.05) %>% arrange(desc(odds.ratio)) 

down_motifs <- fisher_TEtest(motifs_hindra_sigDOWN, motifs_hindra_notsig, simulate = T)
DOWNmotifs.table <- list_to_table(down_motifs)
#DOWNmotifs.table %>% filter(odds.ratio < 0.5 | odds.ratio >2, padj<0.05) %>% arrange(desc(odds.ratio)) 


## fix plotting of motifs 

library(magrittr)

plot_motifs = function(sig.table = DOWNmotifs.table){
  
  # plot first not sig:
  sig.table %>% filter(padj>0.01) %$% 
  plot(counts.notsig, counts.sig , 
       ylim=c(0, max(sig.table$counts.sig, na.rm = T)+20),
       xlim=c(0, max(sig.table$counts.notsig, na.rm = T)+20),
       xlab="Motif counts in non-significant hidra",
       ylab="Motif counts in significant hidra", pch=21, bg=alpha("darkgrey", 0.7))
  
  sig.table %>% filter(padj<0.01) %$% 
    points(counts.notsig, counts.sig , xlab="Motif counts in non-significant hidra",
         ylab="Motif counts in significant hidra", pch=21, bg=alpha("firebrick", 0.7))
  motif.nr <- sig.table %>% filter(padj<0.01) %>% nrow()
  legend('bottomright', bty='n', legend=paste('OR>2 | OR< 0.5 \n padj<0.01 (# motifs', motif.nr, ')'), fill=alpha("firebrick", 0.7))
}

#UPmotifs.table %>% view()
#DOWNmotifs.table %>% view()
```

The results showed that `r sum(UPmotifs.table$padj<0.01, na.rm=T)` TFBS had a different distribution (p<0.01) in MPRA fragments inducing transcription compared to non-active fragments (Figure 3A). `r sum(UPmotifs.table$padj<0.01 & DOWNmotifs.table$odds.ratio > 0, na.rm=T)` TFBS were enriched in transcriptionally inducing MPRA fragments. Among the most enriched motifs (highest odds ratio) were 3 heat shock factor (HSF) motifs, 6 motifs matching FOS/JUN/FOSJUN, as well as a BACH2 motif. `r sum(DOWNmotifs.table$padj<0.01 & DOWNmotifs.table$odds.ratio > 0, na.rm=T)` TFBS (p<0.01) were enriched in transcriptionally repressive MPRA fragments, and among the top enriched TFBS were known transcription repressor binding sites such as [KLF9](https://www.genecards.org/cgi-bin/carddisp.pl?gene=KLF9))......... WHAT MORE??? 

<br> 

```{r, echo=F, fig.width=9, fig.height=6, fig.cap="Figure 3: Enrichment of TFBS in active MPRA fragments. (A) fragments inducing transcription. (B) fragments repressing transcription." }
par(mfrow=c(1,2))
plot_motifs(UPmotifs.table)
mtext(3, line = 0, text = "(A)")
plot_motifs(DOWNmotifs.table)
mtext(3, line = 0, text = "(B)")

pdf(file = 'figures/enriched_motifs.pdf', width = 9, height = 6)
par(mfrow=c(1,2))
plot_motifs(UPmotifs.table)
mtext(3, line = 0, text = "(A)")
plot_motifs(DOWNmotifs.table)
mtext(3, line = 0, text = "(B)")
dev.off()

```

#### The numbers and effect sizes of TE-CREs from super-spreading TE families 
The enrichment of active MPRA-fragments from specific TE families suggests that some TEs holds increased potential for spreading novel cis-regulatory elements across the genome. To further look into these 'super-spreaders' we performed an in depth analyses of the genomic distribution and effect sizes of MPRA fragments from the super-spreading families. 

<br>

```{r, echo=FALSE, fig.cap='Figure 4: MPRA results from specific TE families.', fig.width=7, fig.height=7}

enriched <- sub('.*_', '', colnames(family.barplot.table.ready))
overlap$family <- overlap %>% pull(X7) %>% sub(".*_", "", .) 
overlap$family.ridge <- overlap$family
overlap$family.ridge[!overlap$family %in% enriched] <- "not-enriched"


library(ggplot2)
library(ggridges)
#install.packages('cowplot')
library(cowplot)

# overlap.unique$family.ridge
upFams <- names(which(family.barplot.table$padjUP<0.05))
DownFams <- names(which(family.barplot.table$padjDOWN<0.05))

#overlap %>% filter(family %in% "SsaEle0431")

overlap.unique <- overlap %>% distinct(hidraID, .keep_all=T)
overlap.unique$cols <- "Enriched in repressive MPRA"
overlap.unique$cols[overlap.unique$family.ridge %in% "not-enriched"] <- "Others"
overlap.unique$cols[overlap.unique$family.ridge %in% upFams] <- "Enriched in induced MPRA"

# overlap.unique %>% filter(family %in% "SsaEle0431") # missing samples in B have very few observations... 

p1 <- overlap.unique %>% ggplot(aes(x = X11, y = family.ridge, fill=cols, alpha=0.5)) +
                          stat_density_ridges(quantile_lines = TRUE, quantiles = 2) 

overlap.unique.repressive <- overlap.unique %>% filter(cols %in% "Enriched in repressive MPRA") %>% filter(X12<0.05)
overlap.unique.inductive <- overlap.unique %>% filter(cols %in% "Enriched in induced MPRA") %>% filter(X12<0.05)

overlap.unique.other <- overlap.unique %>% filter(cols %in% "Others") %>% filter(X12<0.05)


p2 <- rbind(overlap.unique.repressive, overlap.unique.inductive, overlap.unique.other) %>% ggplot(aes(x = X11, y = family.ridge, fill=cols, alpha=0.5)) +
                          stat_density_ridges(quantile_lines = TRUE, quantiles = 2) 

## HIT: FIX 

# Warning message: running Line 471
# In family.mprafrags.sign$count/family.mprafrags$count :
#   longer object length is not a multiple of shorter object length

# active frags per TE family
family.mprafrags <- overlap.unique %>% filter(!cols %in% "Others") %>% group_by(family) %>% summarise(count=n())
family.mprafrags.sign <- overlap.unique %>% filter(!cols %in% "Others") %>% filter(X11<0.05) %>% group_by(family) %>% summarise(count=n())
family.mprafrags <- family.mprafrags %>% filter(family %in% family.mprafrags.sign$family)
prop.active.frags.TEfam <- family.mprafrags.sign$count/family.mprafrags$count


# library(ggplot2)
# # Basic density
# p <- ggplot(df, aes(x=weight)) + 
#   geom_density()
# p
# # Add mean line
# p+ geom_vline(aes(xintercept=mean(weight)),
#             color="blue", linetype="dashed", size=1)
# 
# 
# 
# 

family.mprafrags.others <- overlap.unique %>% filter(cols %in% "Others") %>% group_by(family) %>% summarise(count=n())
family.mprafrags.others.sign <- overlap.unique %>% filter(cols %in% "Others") %>% filter(X11<0.05) %>% group_by(family) %>% summarise(count=n())

family.mprafrags.others <- family.mprafrags.others %>% filter(family %in% family.mprafrags.others.sign$family) 
prop.active.frags.others <- family.mprafrags.others.sign$count/family.mprafrags.others$count

# plot(density(prop.active.frags.others))
# points(density(prop.active.frags.TEfam, col = alpha("firebrick", 0.5), add=T))

```


We started looking at the number of MPRA fragment from TE families. The total number of MPRA fragments coming from super-spreading TE families ranged from `r min(family.mprafrags$count)` (`r family.mprafrags$family[order(family.mprafrags$count)][1]`) to `r max(family.mprafrags$count)` (`r family.mprafrags$family[order(family.mprafrags$count, decreasing=T)][1]`) (Figure 4).

```{r, echo=F, fig.cap="Figure 5. Number of MPRA fragments" }

par(mar=c(6,4,4,4))
barplot.spreaders = right_join(family.mprafrags.sign, family.mprafrags, by='family')
barplot.frags <- t(barplot.spreaders[,-1])
colnames(barplot.frags) <- barplot.spreaders$family
TEsupfam_colnames <- TEsupfam[match(colnames(barplot.frags), TEfam)]
colnames(barplot.frags) <- paste0(TEsupfam_colnames, "_", colnames(barplot.frags))
barplot.frags <- barplot.frags[,c(order(TEsupfam_colnames))]
barplot(barplot.frags,  las=3)
legend('topright', bty='n', legend=c('significant MPRA', 'non-significant MPRA'), fill=c('black', 'lightgrey'))

```


<br>




```{r, echo=F}

write_delim(UPfamily_table, delim = "\t", file = "results_files/family_enrichment_inducing.tsv")

write_delim(DOWNfamily_table, delim = "\t", file = "results_files/family_enrichment_repressing.tsv")

write_delim(DOWN_fisher.table, delim = "\t", file = "results_files/superfamily_enrichment_repressing.tsv")

write_delim(UP_fisher.table, delim = "\t", file = "results_files/superfamily_enrichment_inducing.tsv")

write_delim(UPmotifs.table, delim = "\t", file = "results_files/motifs_enrichment_inducing.tsv")

write_delim(DOWNmotifs.table, delim = "\t", file = "results_files/motifs_enrichment_repressing.tsv")



```

<br>

### Significant superfamilies fisher-test

```{r, echo=F}

#install.packages("DT")
library(DT)

UP_fisher.table$direction <- "UP"
DOWN_fisher.table$direction <- "DOWN"
datatable(drop_na(rbind(UP_fisher.table, DOWN_fisher.table)))


```

<br>

### Significant families fisher-test

```{r, echo=F}

UPfamily_table$direction <- "UP"
DOWNfamily_table$direction <- "DOWN"
datatable(drop_na(rbind(UPfamily_table, DOWNfamily_table)))

```

<br>

### Significant motifs fisher-test


```{r, echo=F}

UPmotifs.table$direction <- "UP"
DOWNmotifs.table$direction <- "DOWN"
datatable(drop_na(rbind(UPmotifs.table, DOWNmotifs.table)))

```
